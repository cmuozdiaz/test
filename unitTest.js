let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= '0.0.0.0:3000';



describe('Unit test',()=>{
    const random = Math.floor(Math.random() * (99999 - 10000) + 10000);
    const email = `correo${random}@gmail.com`;
    const password = `${random}password`;
    let token = "";
    let taskId = "";
    let manualTaskId = "";
    let projectId = "";

    it('Create a user', (done) => {
        chai.request(url)
            .post('/user/create')
            .send({email: email, password: password})
            .end( function(err,res){
                token = res.body.user.token;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Login', (done) => {
        chai.request(url)
            .post('/user/login')
            .send({email: email, password: password})
            .end( function(err,res){
                token = res.body.token;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Login with error', (done) => {
        chai.request(url)
            .post('/user')
            .send({email: "otrocoreo@gmail.com", password: password})
            .end( function(err,res){
                expect(res).to.have.status(404);
                done();
            });
    });

    it('Create a task', (done) => {
        chai.request(url)
            .post('/task')
            .set('Authorization', token)
            .send({name: "test_task"})
            .end( function(err,res){
                taskId = res.body._id;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Create a manual task', (done) => {
        chai.request(url)
            .post('/task')
            .set('Authorization', token)
            .send({ duration: 1000, name: "test_task_manual"})
            .end( function(err,res){
                manualTaskId = res.body._id;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Get tasks list', (done) => {
        chai.request(url)
            .get('/task')
            .set('Authorization', token)
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });


    it('Pause task', (done) => {
        chai.request(url)
            .put('/task')
            .set('Authorization', token)
            .send({ _id: taskId, status: "Paused"})
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Resume task', (done) => {
        chai.request(url)
            .put('/task')
            .set('Authorization', token)
            .send({_id: taskId, status: "Running"})
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Finish task', (done) => {
        chai.request(url)
            .put('/task')
            .set('Authorization', token)
            .send({_id: taskId, status: "Finished"})
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Create project', (done) => {
        chai.request(url)
            .post('/project')
            .set('Authorization', token)
            .send({ name: "Test project"})
            .end( function(err, res){
                projectId = res.body._id;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Get project list', (done) => {
        chai.request(url)
            .get('/project')
            .set('Authorization', token)
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Update project', (done) => {
        chai.request(url)
            .put('/project')
            .set('Authorization', token)
            .send({_id: projectId, name: "New name"})
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Associate task to project', (done) => {
        chai.request(url)
            .post('/project/add_task')
            .set('Authorization', token)
            .send({_id: projectId, task_id: taskId})
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });


    it('Delete a task', (done) => {
        chai.request(url)
            .delete('/task')
            .set('Authorization', token)
            .send({_id: taskId})
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Delete project', (done) => {
        chai.request(url)
            .delete('/project')
            .set('Authorization', token)
            .send({_id: projectId})
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Delete user', (done) => {
        chai.request(url)
            .delete('/user')
            .set('Authorization', token)
            .end( function(err,res){
                expect(res).to.have.status(200);
                done();
            });
    });


});

