const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');


const indexRouter = require('./routes/index');
const usersRouter = require('./apps/user');
const taskRouter = require('./apps/task');
const projectRouter = require('./apps/project');


const app = express();

// DB
mongoose.connect('mongodb://test:test123@ds135456.mlab.com:35456/dbtest', {useNewUrlParser: true});
const User = mongoose.model('User');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Check login
app.use('*', (req, res, next) => {
    //Check login only for task, project and delete user
    if (req.baseUrl.includes("user") && req.method !== "DELETE"){
        next();
    }else{
        const {authorization} = req.headers;

        User.findOne({
            token: authorization,
        }).exec( (err, user) => {
            if (err) res.status(500).json({ error: true, message: err });
            else {
                if (user !== null){
                    req.user = user;
                    next();
                } else {
                    res.status(401).json({
                        error:true,
                        message: "Please, verify token and try again"
                    });
                }
            }
        });
    }
});
app.use('/', indexRouter);
app.use(usersRouter);
app.use(taskRouter);
app.use(projectRouter);

module.exports = app;
