const _ = require('lodash');

module.exports.formattedDuration = (tasks) => {

    let duration = 0;
    _.forEach(tasks,  (task) => {
        if (task.status === "Running"){
            const createdAt_lastResumed = task.lastResumed || task.createdAt;
            duration += task.duration + (new Date() - createdAt_lastResumed)/1000;
        } else duration += task.duration;
    });
    const hours = String(Math.floor(duration / 3600)).padStart(2, "0");
    const minutes = String(Math.floor((duration % 3600) / 60)).padStart(2, "0");
    const seconds = String(Math.round((duration % 3600) % 60)).padStart(2, "0");

    return {
        duration: duration,
        formatted: `${hours}:${minutes}:${seconds}`
    };
};