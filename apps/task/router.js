const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const _ = require('lodash');
const Joi = require('joi');
const Task = mongoose.model('Task');
const porjectSchema = require('../project');
const Project = mongoose.model('Project');
const utils = require("../utils");

/**
 * list
 * function for list task
 **/
router.get('/task', (req, res, next) => {
    Task.find({
        user: mongoose.Types.ObjectId(req.user._id)
    }).lean().sort({'createdAt': -1}).exec( (err, tasks) => {
        if (err) res.status(500).json({ error: true, message: err });
        else {
            const listTasks = _.map(tasks, task => {
                const duration = utils.formattedDuration([task]);
                return Object.assign({}, {...task}, {
                    duration: duration.duration,
                    formattedDuration: duration.formatted
                })
            });
            res.status(200).send(listTasks);
        }
    });
});

const finishTask = (task) => {
    return new Promise(function (resolve, reject) {
        if (task.status === "Running"){
            const createdAt_lastResumed = task.lastResumed || task.createdAt;
            task.duration += (new Date() - createdAt_lastResumed)/1000;
        }
        task.status = "Finished";
        task.save( (err, new_task) => {
            if (err){
                reject(err);
            }
            else {
                resolve(new_task);
            }
        });
    });
};

/**
 * create task
 * function for create task
 **/
router.post('/task', (req, res, next) => {
    const {duration} = req.body;
    const baseSchema = Joi.object().keys({
        name: Joi.string(),
        duration: Joi.number()
    }).with("duration", "name");

    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {

        const newTask = new Task({user: req.user._id});

        if (req.body.name !== null) newTask.name = req.body.name;

        //Manual task will be saved as Finished
        if (duration !== null && duration !== undefined) {
            newTask.status = "Finished";
            newTask.duration = duration;
            newTask.finishedAt = new Date();
        }
        newTask.save( (err, task) => {
            if (err) {res.status(500).json({ error: true, message: "Error creating task" })}
            else {
                //If user start another task last "Running" task should be marked as "Finished"
                if (duration === null){
                    //Stop last task if it is running (We will only allow one task running at the same time)
                    Task.findOne({
                        _id: {$ne: task._id}, //This is because we don't want to mark current task as Finished
                        user: req.user._id,
                        status: "Running"
                    },   (err, lastTask) => {
                        if (err) {res.status(500).json({ error: true, message: "Error creating task" })}
                        else {
                            if (lastTask != null){ //If there is running task call function to finish task
                                let promise_result = finishTask(last_task).then(function (result) {
                                    res.status(200).json(task);
                                });
                                promise_result['catch'](function(){
                                    Task.deleteOne(task).then(res.status(500).json({ error: true, message: "Error creating task" }))
                                });
                            } else {
                                res.status(200).json(task)
                            }
                        }
                    });
                } else{
                    //If user create a manual task last running task should continue as running
                    res.status(200).json(task)
                }
            }
        });
    } else {
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }
});

/**
 * update task
 * function for update task
 **/
router.put('/task', (req, res, next) => {

    const baseSchema = Joi.object().keys({
        _id: Joi.string().length(24),
        status: Joi.any().valid("Running", "Paused", "Finished")
    });

    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {


        Task.findOne({
            user: mongoose.Types.ObjectId(req.user._id),
            _id: mongoose.Types.ObjectId(req.body._id)
        }).exec( (err, task) => {
            if (err) {res.status(500).json({ error: true, message: err });}
            else {
                if (task !== null){

                    if (req.body.status === "Running"){
                        //Resume a "Paused" task
                        //Task must be "Paused" in order to resume
                        if (task.status === "Paused"){
                            task.status = "Running"; //Mark task as "Running"
                            task.save( (err, new_task) => {
                                if (err) res.status(500).json({ error: true, message: err});
                                else res.status(200).send(new_task)
                            })
                        } else{
                            res.status(400).json({
                                error: true,
                                message: "You cannot resume a task that is already running or finished"
                            })
                        }
                    } else if (req.body.status === "Paused"){
                        //Paused a running task
                        //Task should be "Running" in order to Paused
                        if (task.status === "Running") {
                            task.status = "Paused"; //Mark task as "Paused"
                            task.save( (err, new_task) => {
                                if (err) res.status(500).json({ error: true, message: err});
                                else res.status(200).send(new_task)

                            })
                        } else{
                            res.status(400).json({
                                error: true,
                                message: "You cannot paused a task that is already paused or finished"
                            })
                        }
                    } else{
                        //Task should be "Running" or "Paused" in order to finished
                        if (task.status === "Running" || task.status === "Paused"){
                            //Call function to finish a task
                            let promise_result = finishTask(task).then(function (result) {
                                res.status(200).json(task);
                            });
                            promise_result['catch'](function(){
                                res.status(500).json({ error: true, message: "Error finishing task" });
                            });
                        } else{
                            res.status(400).json({
                                error: true,
                                message: "You cannot finish a task that is already finished"
                            })
                        }
                    }
                } else {
                    res.status(404).json({
                        error: true,
                        message: "Task not found"
                    });
                }
            }
        });
    } else {
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }
});

/**
 * delete task
 * function for update task
 **/
router.delete('/task', function (req, res, next) {

    const baseSchema = Joi.object().keys({
        _id: Joi.string().length(24)
    });

    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {

        //First remove task from projects
        Project.updateMany({
            user: mongoose.Types.ObjectId(req.user._id),
            tasks: mongoose.Types.ObjectId(req.body._id)
        },{
            $pull:{
                tasks: mongoose.Types.ObjectId(req.body._id)
            }
        },{ multi: true}, function (err, projects) {
            if (err) res.status(500).json({ error: true, message: err });
            else {
                //Now that task is removed from all project we can delete that object
                Task.deleteOne({
                    user: mongoose.Types.ObjectId(req.user._id),
                    _id: mongoose.Types.ObjectId(req.body._id)
                }).exec(function (err, result) {
                    if (err) res.status(500).json({ error: true, message: err });
                    else {
                        if (result.n === 0)res.status(404).json({ error: true, message: "Task not found"});
                        else res.status(200).json({message: "Task deleted successfully"});
                    }
                });
            }
        });
    } else{
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }
});

module.exports = router;