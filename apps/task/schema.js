const mongoose = require('mongoose');


const TaskSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true, },
    name: {  type: String, trim: true, lowercase: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    finishedAt: { type: Date },
    lastPaused: { type: Date },
    lastResumed: { type: Date },
    status: { type: String, default: "Running" },
    duration: { type: Number, default: 0 },
    formattedDuration: { type: String },
});

TaskSchema.pre('save', function(next) {
    if (this.isModified('status') && this.status === "Paused" ) {
        const createdAt_lastResumed = this.lastResumed || this.createdAt;
        this.lastPaused = new Date();
        this.duration += (new Date() - createdAt_lastResumed)/1000;
    }
    if (this.isModified('status') && this.status === "Running" ) {
        this.lastResumed = new Date();
    }
    if (this.isModified('status') && this.status === "Finished" ) {
        this.finishedAt = new Date();
    }
    next();

});


mongoose.model('Task', TaskSchema, "Task");