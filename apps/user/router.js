const express = require('express');
const router = express.Router();
const Joi = require('joi');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const taskSchema = require('../task');
const Task = mongoose.model('Task');
const porjectSchema = require('../project');
const Project = mongoose.model('Project');

/**
 * Login
 * function for login
 * @param {string} email       Required, email of user
 * @param {string} password    Required, password of user
 **/
router.post('/user/login', (req, res, next) => {
    const {email, password} = req.body;
    //Find the user
    User.findOne({ email }).exec( (err, user) => {
        if (err) {
            res.status(400).json({ error: true, message: err });
        } else {
            if (user !== null){
                //If user exist, compare password with save password in db
                user.verifyPassword(password, (err, isMatch ) => {
                    if (err) {
                        res.status(500).json({ error: true, message: "Please, verify email and password and try again" });
                    } else if(isMatch === false){
                        res.status(400).json({
                            error:true,
                            message: "Please, verify email and password and try again"
                        });
                    } else {
                        res.status(200).json({token: user.token})
                    }
                });
            } else {
                res.status(400).json({
                    error:true,
                    message: "Please, verify email and password and try again"
                });
            }
        }
    });
});

/**
 * create
 * function for login
 * @param {string} email       Required, email of user
 * @param {string} password    Required, password of user
 **/
router.post('/user/create', (req, res, next) => {
    const baseSchema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required().min(8)
    });
    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {
        const user_to_save = new User({...req.body});
        user_to_save.save( (err, user) => {
            console.log(err)
            if (err) {
                if (err.code = 11000){
                    res.status(500).json({ error: true, message: "Email already registered!" });
                } else {
                    res.status(500).json({ error: true, message: err });
                }
            } else {
                res.status(200).json({user});
            }
        });
    }
    else{
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }
});

router.delete('/user', (req, res, next) => {

    User.deleteOne({
        token:req.user.token,
    }).exec( (err, result) => {
        if (err) res.status(500).json({ error: true, message: err });
        else {
            if (result.n === 0)res.status(404).json({ error: true, message: "User not found"});
            else {
                Project.deleteMany({
                    user: mongoose.Types.ObjectId(req.user._id)
                }).exec( (err, result) => {
                    Task.deleteMany({
                        user: mongoose.Types.ObjectId(req.user._id)
                    }).exec( (err, result) => {
                        res.status(200).json({message: "User deleted successfully"});
                    });
                })
            }
        }
    });
});

module.exports = router;