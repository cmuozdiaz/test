const schema = require("./schema");
const router = require("./router");

module.exports = router;