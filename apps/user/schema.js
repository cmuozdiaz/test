const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const uuidv1 = require('uuid/v1');

const UserSchema = new mongoose.Schema({
    email: { type: String, trim: true, required: true, lowercase: true },
    password: { type: String, required: true },
    token: { type: String, default: uuidv1() },
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});


//Encrypt password
UserSchema.pre('save', function(next)  {
    this.updatedAt = Date.now;
    console.log(this)
    if (!this.isModified('password')) return next();
    this.password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(8), null);
    next();

});

//Verify password
UserSchema.methods.verifyPassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

mongoose.model('User', UserSchema, "User");