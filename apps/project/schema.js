const mongoose = require('mongoose');


const ProjectSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true, },
    name: {  type: String, trim: true, lowercase: true },
    tasks: Array,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

mongoose.model('Project', ProjectSchema, "Project");