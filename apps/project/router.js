const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Project = mongoose.model('Project');
const taskSchema = require('../task');
const Task = mongoose.model('Task');

const _ = require('lodash');
const Joi = require('joi');
const utils = require("../utils");


/**
 * list
 * function for list porject
 **/
router.get('/project', (req, res, next) => {

    Project.aggregate([
        {
            $match: {
                user: mongoose.Types.ObjectId(req.user._id)
            },
        },
        {
            $lookup: {
                from: "Task",
                localField: "tasks",
                foreignField: "_id",
                as: "tasks"
            }
        }
    ]).exec( (err, projects)=> {
        if (err) res.status(500).json({ error: true, message: err });
        else {
            const listProjects = _.map(projects, project => {
                const duration = utils.formattedDuration(project.tasks);
                return Object.assign({}, {...project}, {
                    duration: duration.duration,
                    formattedDuration: duration.formatted
                })
            });
            res.status(200).send(listProjects);
        }
    });
});

/**
 * create
 * function for create porject
 **/
router.post('/project', (req, res, next) =>  {

    const baseSchema = Joi.object().keys({
        name: Joi.string().required(),
    });

    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {

        const project_to_save = new Project({
            name: req.body.name,
            user: req.user._id
        });

        project_to_save.save( (err, project) => {
            if (err) res.status(500).json({ error: true, message: err });
            else res.status(200).json(project);
        });

    }  else{
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }

});


/**
 * update
 * function for create porject
 **/
router.put('/project', (req, res, next) => {


    const baseSchema = Joi.object().keys({
        name: Joi.string().required(),
        _id: Joi.string().required().length(24),});

    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {
        //Look project and then update in same db call
        Project.findOneAndUpdate({
            user: mongoose.Types.ObjectId(req.user._id),
            _id: mongoose.Types.ObjectId(req.body._id)
        },{
            $set: {
                name: req.body.name,
                updatedAt: new Date()
            }
        },{new: true},  (err, project) => {
            if (err) res.status(500).json({ error: true, message: err });
            else {
                if (project == null) res.status(404).json({ error: true, message: "Project not found"});
                else res.status(200).json(project);
            }
        })
    } else {
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }
});

/**
 * update
 * function for create porject
 **/
router.delete('/project',  (req, res, next) => {

    const baseSchema = Joi.object().keys({
        _id: Joi.string().length(24)
    });

    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {

        Project.deleteOne({
            user: mongoose.Types.ObjectId(req.user._id),
            _id: mongoose.Types.ObjectId(req.body._id)
        }).exec( (err, result) => {
            if (err) res.status(500).json({ error: true, message: err });
            else {
                if (result.n === 0) res.status(404).json({ error: true, message: "Project not found"});
                else res.status(200).json({message: "Project deleted successfully"})
            }
        });
    }
    else{
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }
});

/**
 * update
 * function for add task to porject
 **/
router.post('/project/add_task', function (req, res, next) {

    const baseSchema = Joi.object().keys({
        task_id: Joi.string().required().length(24),
        _id: Joi.string().required().length(24)
    });

    const data = req.body;
    const validationResult = Joi.validate(data, baseSchema);

    if (!validationResult.error) {
        // First we search for project
        Project.findOne({
            user: mongoose.Types.ObjectId(req.user._id),
            _id: mongoose.Types.ObjectId(req.body._id)
        },  (err, project) => {
            if (err) res.status(500).json({ error: true, message: err });
            else{
                if (project == null) res.status(404).json({ error: true, message: "Project not found"});
                else {
                    //if project exists we search for task
                    Task.findOne({
                        user: mongoose.Types.ObjectId(req.user._id),
                        _id: mongoose.Types.ObjectId(req.body.task_id)
                    }).select('_id').exec(function (err, task) {
                        if (task === null) res.status(404).json({ error: true, message: "Task not found"});
                        else {
                            // If task exists we add task to project using $addToSet because it prevent duplicate task_id in id array
                            Project.findOneAndUpdate({
                                user: mongoose.Types.ObjectId(req.user._id),
                                _id: mongoose.Types.ObjectId(req.body._id)
                            }, {
                                $set:{
                                    updatedAt: new Date()
                                },
                                $addToSet: {
                                    tasks: task._id
                                },
                            },{new: true},function (err,  new_project) {
                                if (err) res.status(500).json({ error: true, message: err });
                                else res.status(200).json(new_project);
                            });
                        }
                    });
                }
            }
        });
    }
    else {
        res.status(400).json({
            error: true,
            errorMessage: validationResult.error.details[0].message
        });
    }
});

module.exports = router;